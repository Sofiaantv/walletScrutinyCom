---
wsId: atomic
title: "Atomic Wallet"
altTitle: 

appId: atomicwallet
idd: 1478257827
released: 2019-11-05
updated: 2021-02-11
version: "0.71.3"
score: 4.44739
reviews: 4723
size: 47784960
developerWebsite: https://atomicwallet.io/
repository: 
issue: 
icon: atomicwallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: atomicwallet
providerLinkedIn: 
providerFacebook: atomicwallet
providerReddit: 

redirect_from:

---

> Atomic Wallet is a universal, fully decentralized, multi-currency, and
  convenient app with a simple interface that supports over 300
  cryptocurrencies.

so they claim to be non-custodial but although they feature a link to
[their GitHub account](https://github.com/Atomicwallet), none of the
repositories there looks like an iPhone wallet so the app is **not verifiable**.