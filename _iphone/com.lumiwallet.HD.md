---
wsId: Lumi Wallet
title: "Bitcoin Wallet by Lumi Wallet"
altTitle:

appId: com.lumiwallet.HD
idd: 1316477906
released: 2017-12-08
updated: 2021-02-18
version: "3.9.6"
score: 4.8521
reviews: 2887
size: 75776000
developerWebsite: https://lumiwallet.com/
repository:
issue:
icon: com.lumiwallet.HD.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
