---
wsId: nexo
title: "Nexo - Crypto Banking Account"
altTitle:

appId: com.nexobank.wallet
idd: 1455341917
released: 2019-06-30
updated: 2021-02-18
version: "1.3.3"
score: 4.0625
reviews: 224
size: 34237440
developerWebsite: https://nexo.io
repository:
issue:
icon: com.nexobank.wallet.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
