---
wsId: casaapp
title: "Casa App - Secure your Bitcoin"
altTitle: 

appId: com.casa.vault
idd: 1314586706
released: 2018-08-02
updated: 2021-02-12
version: "2.32.1"
score: 4.93137
reviews: 204
size: 42533888
developerWebsite: https://keys.casa
repository: 
issue: 
icon: com.casa.vault.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

