---
wsId: BlueWallet - Bitcoin wallet
title: "BlueWallet - Bitcoin wallet"
altTitle:

appId: io.bluewallet.bluewallet
idd: 1376878040
released: 2018-05-27
updated: 2021-02-12
version: "6.0.4"
score: 4.20442
reviews: 181
size: 65783808
developerWebsite:
repository:
issue:
icon: io.bluewallet.bluewallet.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
