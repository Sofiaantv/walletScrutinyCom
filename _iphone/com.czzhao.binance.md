---
wsId: binance
title: "Binance: Buy Bitcoin Securely"
altTitle: 

appId: com.czzhao.binance
idd: 1436799971
released: 2018-10-06
updated: 2021-02-09
version: "2.25.2"
score: 4.52497
reviews: 32297
size: 450238464
developerWebsite: https://www.binance.com
repository: 
issue: 
icon: com.czzhao.binance.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binance
providerLinkedIn: 
providerFacebook: binance
providerReddit: binance

redirect_from:

---

In the description the provider claims:

> Your funds are protected by our Secure Asset Fund for Users (SAFU Funds) which
  means we have your back.

which sounds very custodial and as such the app is **not verifiable**.
