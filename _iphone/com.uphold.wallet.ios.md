---
wsId: Uphold: buy and sell Bitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle:

appId: com.uphold.wallet.ios
idd: 1101145849
released: 2016-04-19
updated: 2021-02-13
version: "4.15.9"
score: 4.11679
reviews: 3596
size: 63809536
developerWebsite:
repository:
issue:
icon: com.uphold.wallet.ios.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
