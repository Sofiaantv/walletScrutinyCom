---
wsId: OWNR
title: "OWNR crypto wallet for PC"
altTitle:

appId: com.ownrwallet.desktop
idd: 1520395378
released: 2020-08-13
updated: 2021-02-10
version: "1.3.6"
score:
reviews:
size: 122484433
developerWebsite: https://ownrwallet.com
repository:
issue:
icon: com.ownrwallet.desktop.png
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
