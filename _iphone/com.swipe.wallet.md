---
wsId: Swipe Wallet
title: "Swipe Wallet"
altTitle:

appId: com.swipe.wallet
idd: 1476726454
released: 2019-09-10
updated: 2021-02-15
version: "1.530"
score: 4.77935
reviews: 1201
size: 141314048
developerWebsite: https://swipe.io
repository:
issue:
icon: com.swipe.wallet.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
