---
wsId: kucoin
title: "KuCoin"
altTitle:

appId: com.kucoin.KuCoin.iOS
idd: 1378956601
released: 2018-05-14
updated: 2021-02-10
version: "3.27.0"
score: 3.70181
reviews: 332
size: 196850688
developerWebsite:
repository:
issue:
icon: com.kucoin.KuCoin.iOS.jpg
bugbounty:
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: true
signer:
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: company/kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---

> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
