---
wsId: bitpaywallet
title: "BitPay – Buy Crypto"
altTitle: 

appId: com.bitpay.wallet
idd: 1149581638
released: 2016-10-24
updated: 2021-02-12
version: "12.1.0"
score: 4.04845
reviews: 1032
size: 82147328
developerWebsite: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

