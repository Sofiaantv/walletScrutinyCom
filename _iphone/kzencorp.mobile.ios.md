---
wsId: ZenGo
title: "ZenGo: Crypto & Bitcoin Wallet"
altTitle:

appId: kzencorp.mobile.ios
idd: 1440147115
released: 2019-06-07
updated: 2021-02-14
version: "2.21.0"
score: 4.62152
reviews: 1041
size: 71142400
developerWebsite: https://www.zengo.com
repository:
issue:
icon: kzencorp.mobile.ios.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
