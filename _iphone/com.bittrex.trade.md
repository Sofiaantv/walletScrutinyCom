---
wsId: bittrex
title: "Bittrex"
altTitle: 

appId: com.bittrex.trade
idd: 1465314783
released: 2019-12-19
updated: 2021-02-02
version: "1.13.1"
score: 2.3913
reviews: 207
size: 62560256
developerWebsite: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: false
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---

This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
