---
wsId: ZelCore
title: "ZelCore"
altTitle:

appId: com.zelcash.zelcore
idd: 1436296839
released: 2018-09-23
updated: 2021-02-17
version: "v4.5.0"
score: 4.48
reviews: 50
size: 62567424
developerWebsite: https://zel.network/zelcore
repository:
issue:
icon: com.zelcash.zelcore.jpg
bugbounty:
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
