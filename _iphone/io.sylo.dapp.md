---
wsId: Sylo
title: "Sylo"
altTitle:

appId: io.sylo.dapp
idd: 1452964749
released: 2019-09-10
updated: 2021-02-16
version: "3.0.9"
score: 4.54545
reviews: 11
size: 182219776
developerWebsite: https://www.sylo.io/wallet/
repository:
issue:
icon: io.sylo.dapp.jpg
bugbounty:
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
