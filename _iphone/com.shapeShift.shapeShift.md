---
wsId: ShapeShift
title: "ShapeShift: Buy & Trade Crypto"
altTitle:

appId: com.shapeShift.shapeShift
idd: 996569075
released: 2015-06-09
updated: 2021-02-12
version: "2.11.0"
score: 3.03601
reviews: 361
size: 77678592
developerWebsite: https://shapeshift.com
repository:
issue:
icon: com.shapeShift.shapeShift.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
