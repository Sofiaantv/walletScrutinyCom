---
wsId: klever
title: "Klever: Crypto Bitcoin Wallet"
altTitle: 

appId: cash.klever.blockchain.wallet
idd: 1525584688
released: 2020-08-26
updated: 2021-02-17
version: "4.1.5"
score: 4.50753
reviews: 199
size: 119495680
developerWebsite: https://klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

