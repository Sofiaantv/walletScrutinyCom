---
wsId: 
title: "Enjin Crypto Blockchain Wallet"
altTitle: 

appId: com.enjin.mobile.wallet
idd: 1349078375
released: 2018-03-12
updated: 2021-02-08
version: "1.12.0"
score: 4.61521
reviews: 434
size: 43020288
developerWebsite: https://enjin.io/products/wallet
repository: 
issue: 
icon: com.enjin.mobile.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

