---
wsId: Celsius
title: "Celsius - Crypto Wallet"
altTitle:

appId: network.celsius.wallet
idd: 1387885523
released: 2018-06-20
updated: 2021-01-25
version: "4.7.0"
score: 4.2721
reviews: 735
size: 43788288
developerWebsite: https://celsius.network/app
repository:
issue:
icon: network.celsius.wallet.jpg
bugbounty:
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
