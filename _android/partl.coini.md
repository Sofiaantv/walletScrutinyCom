---
title: "Coini — Bitcoin / Cryptocurrencies"
altTitle: 

users: 1000
appId: partl.coini
launchDate: 
latestUpdate: 2020-11-24
apkVersionName: "1.9.5"
stars: 4.6
ratings: 135
reviews: 71
size: 45M
website: 
repository: 
issue: 
icon: partl.coini.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /partl.coini/
---


This app is for portfolio tracking but probably is not in control of private keys.