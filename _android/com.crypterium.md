---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 

users: 100000
appId: com.crypterium
launchDate: 
latestUpdate: 2021-02-16
apkVersionName: "2.6.39.10"
stars: 3.9
ratings: 6052
reviews: 3263
size: 38M
website: https://crypterium.com
repository: 
issue: 
icon: com.crypterium.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: crypterium
providerLinkedIn: 
providerFacebook: crypterium.org
providerReddit: 

redirect_from:
  - /com.crypterium/
---


This app is a custodial offering with many many users complaining about never
having been able to get their funds out. The app is **not verifiable**.
