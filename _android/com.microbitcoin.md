---
title: "MBC Wallet - MicroBitcoin Wallet"
altTitle: 

users: 1000
appId: com.microbitcoin
launchDate: 2018-10-30
latestUpdate: 2020-01-23
apkVersionName: "2.0.1"
stars: 3.8
ratings: 34
reviews: 20
size: 9.9M
website: https://microbitcoin.org
repository: 
issue: 
icon: com.microbitcoin.png
bugbounty: 
verdict: nobtc # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: MicroBitcoinOrg
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.microbitcoin/
  - /posts/com.microbitcoin/
---


