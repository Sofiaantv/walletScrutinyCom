---
title: "BitBoxApp"
altTitle: 

users: 1000
appId: ch.shiftcrypto.bitboxapp
launchDate: 
latestUpdate: 2020-12-12
apkVersionName: "android-4.24.1"
stars: 4.6
ratings: 14
reviews: 8
size: 63M
website: https://shiftcrypto.ch/app
repository: https://github.com/digitalbitbox/bitbox-wallet-app
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: false
signer: 
reviewArchive:


providerTwitter: ShiftCryptoHQ
providerLinkedIn: company/shift-crypto
providerFacebook: Shiftcrypto
providerReddit: 

redirect_from:
---


The description of this app reads:

> A BitBox02 hardware wallet is required.

so we assume that this app does not manage private keys or send transactions if
not approved via the hardware wallet. It itself is **not a wallet**.
