---
title: "Decrypt - Bitcoin & crypto news"
altTitle: 

users: 10000
appId: co.decrypt.app
launchDate: 
latestUpdate: 2020-10-07
apkVersionName: "1.1.5"
stars: 4.6
ratings: 165
reviews: 66
size: 28M
website: 
repository: 
issue: 
icon: co.decrypt.app.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.decrypt.app/
---


This app only provides news about Bitcoin but no wallet itself.