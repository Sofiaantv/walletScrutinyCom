---
title: "Sats App by Casa"
altTitle: 

users: 1000
appId: com.satsapp
launchDate: 
latestUpdate: 2020-04-13
apkVersionName: "1.3.1"
stars: 3.1
ratings: 14
reviews: 8
size: 29M
website: https://keys.casa
repository: 
issue: 
icon: com.satsapp.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-07-31
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.satsapp/
  - /posts/com.satsapp/
---


This app was removed from Google Play.