---
title: "Algorand Wallet"
altTitle: 

users: 10000
appId: com.algorand.android
launchDate: 
latestUpdate: 2020-12-01
apkVersionName: "4.5.3"
stars: 4.5
ratings: 170
reviews: 99
size: 34M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---
