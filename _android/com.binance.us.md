---
wsId: Binance.US
title: "Binance.US"
altTitle:

users: 100000
appId: com.binance.us
launchDate:
latestUpdate: 2021-02-10
apkVersionName: "2.3.2"
stars: 2.2
ratings: 1815
reviews: 1351
size: Varies with device
website: https://www.binance.us
repository:
issue:
icon: com.binance.us.png
bugbounty:
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer:
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: company/binance-us
providerFacebook: BinanceUS
providerReddit:

redirect_from:
  - /com.binance.us/
---


Binance being a big exchange, the description on Google Play only mentions
security features like FDIC insurance for USD balance but no word on
self-custody. Their website is not providing more information neither. We
assume the app is a custodial offering and therefore **not verifiable**.
