---
wsId: huobi
title: "Huobi Global"
altTitle:

users: 1000000
appId: pro.huobi
launchDate:
latestUpdate: 2021-02-08
apkVersionName: "6.1.5"
stars: 4.7
ratings: 8014
reviews: 2929
size: 63M
website: https://www.huobi.com/en-us
repository:
issue:
icon: pro.huobi.png
bugbounty:
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer:
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn:
providerFacebook: huobiglobalofficial
providerReddit:

redirect_from:
  - /pro.huobi/
  - /posts/pro.huobi/
---


Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
