---
title: "Neo Bitcoin Wallet"
altTitle: 

users: 1000
appId: com.neobitcoin.wallet
launchDate: 
latestUpdate: 2020-07-03
apkVersionName: "1.0.0"
stars: 1.4
ratings: 113
reviews: 102
size: 48M
website: 
repository: 
issue: 
icon: com.neobitcoin.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.neobitcoin.wallet/
---


Apart from not being a Bitcoin wallet (only Neo Bitcoin), this is also the
wallet app with the lowest rating on Google Play (1.4 stars) that we have ever
come across.
