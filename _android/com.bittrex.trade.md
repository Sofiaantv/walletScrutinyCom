---
wsId: bittrex
title: "Bittrex Global"
altTitle: 

users: 100000
appId: com.bittrex.trade
launchDate: 
latestUpdate: 2021-02-01
apkVersionName: "1.13.1"
stars: 2.4
ratings: 977
reviews: 637
size: 53M
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: false
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---


This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
