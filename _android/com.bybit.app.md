---
wsId: bybit
title: "Bybit: Crypto Trading & Bitcoin Futures App"
altTitle: 

users: 100000
appId: com.bybit.app
launchDate: 
latestUpdate: 2021-02-05
apkVersionName: "1.15"
stars: 4.0
ratings: 1676
reviews: 804
size: 43M
website: https://www.bybit.com
repository: 
issue: 
icon: com.bybit.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: false
signer: 
reviewArchive:


providerTwitter: Bybit_Official
providerLinkedIn: company/bybitexchange
providerFacebook: Bybit
providerReddit: Bybit

redirect_from:

---


> "Bybit is the World's fastest-growing and one of the largest crypto
  derivatives exchanges to trade Bitcoin and crypto.

and as such, funds are in cold storage with them:

> YOUR SAFETY IS OUR PRIORITY<br>
  We safeguard your cryptocurrencies with a multi-signature cold-wallet
  solution. Your funds are 100% protected from the prying eyes. All traders'
  deposited assets are segregated from Bybit's operating budget to increase our
  financial accountability and transparency.

As a custodial app it is **not verifiable**.
